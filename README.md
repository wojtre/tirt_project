# README #

### What is this repository for? ###

Program na projekt z TIRT. Pozwala na wczytanie filmu w formacie .avi i późniejsze jego przetwarzanie w modułach
połączonych poprzez komunikację opartą o protokół UDP.

### How do I get set up? ###

* Należy pobrać OpenCV w wersji 2.4.10. Rozpakować. Następnie dodać do projektu bibliotekę opencv-2411.jar lub opencv-2410.jar.

* Następnie należy w klasie DecodeAndPlayVideo dodać swoją ścieżkę do biblioteki natywnej .dll

OPENCV_PATH_WOJTEK = "C:\\Program Files\\Java\\opencv\\build\\java\\x64\\opencv_java2411.dll";

OPENCV_PATH_ADRIAN = "C:\\Users\\Adrian\\Documents\\opencv\\build\\java\\x86\\opencv_java2410.dll";

* należy też dodać ścieżkę do filmu .avi

MOVIE_PATH_WOJTEK = "C:/Users/wojci/IdeaProjects/license-plate-recognition-system/src/main/resources/filmy/film1.avi";
MOVIE_PATH_ADRIAN = "drop2.avi";

* na sam koniec należy podmienić u siebie stałe np w taki sposób:

private static final String MOVIE_PATH = OPENCV_PATH_WOJTEK;
private static final String OPENCV_PATH = MOVIE_PATH_WOJTEK;

### Who do I talk to? ###

Wojciech Trefon
Adrian Wosiński
Arnold Woźnica