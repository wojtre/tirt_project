package Processing;

public enum ImageProcessType {
	PREWITT, SOBEL, KIRSCH, LAPLACIAN, FLIP_X, FLIP_Y, FLIP_XY, BASIC_THRESHOLD, ZOOM;
}
