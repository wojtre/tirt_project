package Processing;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class ProcessOperationsBuilder {
	private static final int ZOOM_FACTOR = 2;
	private List<ImageProcessType> operations = new ArrayList<>();
	private Mat imageToProcess = new Mat();

	private static final Mat SOBEL_KERNEL = new Mat(9, 9, CvType.CV_32F) {
		{
			put(0, 0, -1);
			put(0, 1, 0);
			put(0, 2, 1);

			put(1, 0 - 2);
			put(1, 1, 0);
			put(1, 2, 2);

			put(2, 0, -1);
			put(2, 1, 0);
			put(2, 2, 1);
		}
	};

	private static final Mat PREWITT_KERNEL = new Mat(9, 9, CvType.CV_32F) {
		{
			put(0, 0, -1);
			put(0, 1, 0);
			put(0, 2, 1);

			put(1, 0 - 1);
			put(1, 1, 0);
			put(1, 2, 1);

			put(2, 0, -1);
			put(2, 1, 0);
			put(2, 2, 1);
		}
	};

	private static final Mat KIRSCH_KERNEL = new Mat(9, 9, CvType.CV_32F) {
		{
			put(0, 0, -3);
			put(0, 1, -3);
			put(0, 2, -3);

			put(1, 0 - 3);
			put(1, 1, 0);
			put(1, 2, -3);

			put(2, 0, 5);
			put(2, 1, 5);
			put(2, 2, 5);
		}
	};

	private static final Mat LAPLACIAN_KERNEL = new Mat(9, 9, CvType.CV_32F) {
		{
			put(0, 0, 0);
			put(0, 1, -1);
			put(0, 2, 0);

			put(1, 0 - 1);
			put(1, 1, 4);
			put(1, 2, -1);

			put(2, 0, 0);
			put(2, 1, -1);
			put(2, 2, 0);
		}
	};

	public ProcessOperationsBuilder(Mat imageToProcess) {
		imageToProcess.copyTo(this.imageToProcess);
	}

	public ProcessOperationsBuilder setImageToProcess(Mat imageToProcess) {
		imageToProcess.copyTo(this.imageToProcess);
		return this;
	}

	public ProcessOperationsBuilder addOperation(ImageProcessType processType) {
		operations.add(processType);
		return this;
	}

	public Mat process() {
		for (ImageProcessType type : operations) {
			switch (type) {
				case SOBEL :
					doConvolution(SOBEL_KERNEL);
					break;
				case FLIP_X :
					flipX();
					break;
				case FLIP_Y :
					flipY();
					break;
				case FLIP_XY :
					flipXY();
					break;
				case PREWITT :
					doConvolution(PREWITT_KERNEL);
					break;
				case KIRSCH :
					doConvolution(KIRSCH_KERNEL);
					break;
				case LAPLACIAN :
					doConvolution(LAPLACIAN_KERNEL);
					break;
				case BASIC_THRESHOLD :
					basicThreshold();
					break;
				case ZOOM :
					zoom();
					break;
				default :
					break;
			}
		}
		return imageToProcess;
	}

	private void doConvolution(Mat kernel) {
		Imgproc.filter2D(imageToProcess, imageToProcess, -1, kernel);
	}

	private void flipX() {
		Core.flip(imageToProcess, imageToProcess, 0);
	}

	private void flipY() {
		Core.flip(imageToProcess, imageToProcess, 1);
	}

	private void flipXY() {
		Core.flip(imageToProcess, imageToProcess, -1);
	}

	private void basicThreshold() {
		Imgproc.threshold(imageToProcess, imageToProcess, 127, 255,
				Imgproc.THRESH_BINARY);
	}

	private void zoom() {
		Mat destination = new Mat(imageToProcess.rows() * ZOOM_FACTOR,
				imageToProcess.cols() * 2, imageToProcess.type());
		Imgproc.resize(imageToProcess, destination, destination.size(), 2, 2,
				Imgproc.INTER_NEAREST);
		imageToProcess = destination;

	}
}