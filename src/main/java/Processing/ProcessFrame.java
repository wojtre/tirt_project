package Processing;

import static Processing.ImageProcessType.BASIC_THRESHOLD;
import static Processing.ImageProcessType.FLIP_X;
import static Processing.ImageProcessType.SOBEL;
import static Processing.ImageProcessType.ZOOM;

import java.awt.image.BufferedImage;

import org.opencv.core.Mat;

import Communication.Receiving.BufferedImageReceiver;
import Communication.Receiving.Service;
import Communication.Sending.BufferedImageSender;

public class ProcessFrame implements Service {

	private BufferedImageSender sender;
	private BufferedImageReceiver receiver;

	private static final short CHART_ID = 1; // and chart_id + 2

	public ProcessFrame(int inPort, int outPort) {
		sender = new BufferedImageSender(outPort);
		receiver = new BufferedImageReceiver(inPort, CHART_ID);
		receiver.addService(this);
		new Thread(receiver).start();
	}

	@Override
	public void accept() {
		BufferedImage image = receiver.getImage();
		Mat grayscaleMat = ImageConverter.bufferedImageToGrayscaleMat(image);
		Mat processedImage = new ProcessOperationsBuilder(grayscaleMat)
				.addOperation(ZOOM).addOperation(FLIP_X)
				.addOperation(BASIC_THRESHOLD).addOperation(SOBEL).process();
		image = ImageConverter.mat2BufferedImage(processedImage);
		sender.sendBufferedImage(image);
	}
}
