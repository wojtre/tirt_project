package Communication.Sending;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.List;

public class BufferedImageSender {

	private static DatagramSocket socket;
	private final int outPort;
	private static final int PACKAGE_SIZE = 2000;
	private DataConverter dataConverter = new DataConverter(PACKAGE_SIZE);

	public BufferedImageSender(int outPort) {
		this.outPort = outPort;
		initializeSocket();
	}

	public void sendBufferedImage(BufferedImage bufferedImage) {
		List<byte[]> bytesPackage = dataConverter
				.convertBufferedImageIntoPackages(bufferedImage);
		try {
			for (byte[] array : bytesPackage) {
				socket.send(new DatagramPacket(array, array.length,
						InetAddress.getLocalHost(), outPort));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void initializeSocket() {
		try {
			socket = new DatagramSocket();
			System.out.println("Sender Socket initialized on port: "
					+ socket.getLocalPort());
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
}
