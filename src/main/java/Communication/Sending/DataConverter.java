package Communication.Sending;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

public class DataConverter {
	private int packageSize;
	private byte[] bytes = new byte[0];
	private BufferedImage frame;
	private final static byte[] ONE = new byte[]{1};
	private final static byte[] ZERO = new byte[]{0};

	public DataConverter(int packageSize) {
		this.packageSize = packageSize;
	}

	public List<byte[]> convertBufferedImageIntoPackages(
			BufferedImage bufferedImage) {
		convertBufferedImageToBytes(bufferedImage);
		return divideByteArrayIntoPackages();
	}

	public boolean convertBytesIntoBufferedImage(byte[] data, int length) {

		bytes = concatenate(bytes, Arrays.copyOfRange(data, 1, length));

		if (data[0] == 0) {
			return false;
		}

		InputStream in = new ByteArrayInputStream(bytes);
		try {
			frame = ImageIO.read(in);
		} catch (IOException e) {
			// e.printStackTrace();
		}

		bytes = new byte[0];

		return true;
	}

	public BufferedImage getBufferedImage() {
		return frame;
	}

	private byte[] convertBufferedImageToBytes(BufferedImage bufferedImage) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(bufferedImage, "jpg", baos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		bytes = baos.toByteArray();

		return bytes;
	}

	private List<byte[]> divideByteArrayIntoPackages() {
		List<byte[]> bytesPackages = new ArrayList<>();

		int i;
		for (i = 0; i < bytes.length - packageSize; i += packageSize) {
			bytesPackages.add(concatenate(ZERO,
					Arrays.copyOfRange(bytes, i, i + packageSize)));
		}

		bytesPackages.add(
				concatenate(ONE, Arrays.copyOfRange(bytes, i, bytes.length)));

		return bytesPackages;
	}

	private byte[] concatenate(byte[] first, byte[] second) {
		int aLen = first.length;
		int bLen = second.length;
		byte[] c = (byte[]) Array
				.newInstance(first.getClass().getComponentType(), aLen + bLen);
		System.arraycopy(first, 0, c, 0, aLen);
		System.arraycopy(second, 0, c, aLen, bLen);
		return c;
	}
}
