package Communication.Sending;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class StatDataSending {

	private static final int STAT_RECEIVER_PORT = 9004;

	private DatagramSocket statSocket;

	public StatDataSending() {
		try {
			statSocket = new DatagramSocket();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sendData(int value, short chartId) {
		byte[] statData = getStatData(value, chartId);
		try {
			statSocket.send(new DatagramPacket(statData, statData.length,
					InetAddress.getLocalHost(), STAT_RECEIVER_PORT));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private byte[] getStatData(int value, short chartId) {
		byte[] statData = new byte[6];

		statData[0] = (byte) (chartId & 0xff);
		statData[1] = (byte) ((chartId >> 8) & 0xff);

		statData[2] = (byte) (value & 0xff);
		statData[3] = (byte) ((value >> 8) & 0xff);
		statData[4] = (byte) (value >> 16 & 0xff);
		statData[5] = (byte) ((value >> 24) & 0xff);
		return statData;
	}
}
