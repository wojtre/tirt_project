package Communication.Receiving;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import Communication.Sending.DataConverter;
import Communication.Sending.StatDataSending;

public class BufferedImageReceiver implements Runnable {
	private DatagramSocket datagramSocket;
	private byte[] buf;
	private DatagramPacket datagramPacket;
	private static final int BUFFER_SIZE = 150000;

	private static final long BILION = 1000000000;
	private static final long BITE_SIZE = 8;

	private final int inPort;

	private StatDataSending statDataSending;

	private BufferedImage image;
	private DataConverter dataConverter = new DataConverter(2000);

	private Service service;
	private short chartId;

	public BufferedImageReceiver(int inPort, short chartId) {
		this.inPort = inPort;
		this.chartId = chartId;
		statDataSending = new StatDataSending();
	}

	public void addService(Service service) {
		this.service = service;
	}

	@Override
	public void run() {
		try {
			datagramSocket = new DatagramSocket(inPort);
			buf = new byte[BUFFER_SIZE];
			datagramPacket = new DatagramPacket(buf, buf.length);
			receive();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void receive() throws IOException {
		int bytesSize = 0;
		long timeStart;
		int bitRate;
		timeStart = System.nanoTime();
		while (true) {

			datagramSocket.receive(datagramPacket);

			bytesSize += datagramPacket.getLength();

			if (dataConverter.convertBytesIntoBufferedImage(
					datagramPacket.getData(), datagramPacket.getLength())) {

				bitRate = calculateBitRate(bytesSize, timeStart);
				timeStart = System.nanoTime();

				statDataSending.sendData(bytesSize, chartId);

				statDataSending.sendData(bitRate, (short) (chartId + 2));

				bytesSize = 0;

				image = dataConverter.getBufferedImage();
				if (image != null) {
					service.accept();
				}
			}
		}
	}

	private int calculateBitRate(int bytesSize, long timeStart) {

		long time = (System.nanoTime() - timeStart);
		int bitRate = 0;
		System.out.println("time: " + time + " bytes: " + bytesSize);
		try {
			bitRate = (int) (BITE_SIZE * BILION * bytesSize / time);
		} catch (ArithmeticException e) {

		}
		return bitRate;
	}

	public BufferedImage getImage() {
		return image;
	}
}
