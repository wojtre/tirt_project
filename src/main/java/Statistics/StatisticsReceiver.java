package Statistics;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;

public class StatisticsReceiver implements Runnable {

	private static final int STAT_RECEIVER_PORT = 9004;

	DatagramSocket socket;
	DatagramPacket dgp;

	Map<Short, Chart> chartMap = new HashMap<>();

	byte[] buf;

	public StatisticsReceiver() {
		try {
			socket = new DatagramSocket(STAT_RECEIVER_PORT);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buf = new byte[64];
		dgp = new DatagramPacket(buf, buf.length);
	}

	public void addChart(Chart chart) {
		chartMap.put(chart.getId(), chart);
	}

	@Override
	public void run() {
		short chartId;
		int value;
		while (true) {
			try {
				socket.receive(dgp);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ByteBuffer buffer = ByteBuffer.wrap(dgp.getData())
					.order(ByteOrder.LITTLE_ENDIAN);
			chartId = buffer.getShort();
			value = buffer.getInt();

			chartMap.get(chartId).update(value);
		}
	}
}
