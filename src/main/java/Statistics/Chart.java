package Statistics;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class Chart extends JPanel {

	private static final long serialVersionUID = -2729244992217195285L;
	@SuppressWarnings("deprecation")
	private TimeSeries ts = new TimeSeries("data", Millisecond.class);
	private final JFreeChart chart;
	private short id;
	private static final double AUTO_RANGE = 60000.0;

	public Chart(final String title, final String xAxis, final String yAxis,
			short id, int xPos, int yPos) {

		this.id = id;
		TimeSeriesCollection dataset = new TimeSeriesCollection(ts);
		chart = ChartFactory.createTimeSeriesChart(title, xAxis, yAxis, dataset,
				true, true, false);
		final XYPlot plot = chart.getXYPlot();

		ValueAxis axis = plot.getDomainAxis();

		axis.setFixedAutoRange(AUTO_RANGE);

		add(new ChartPanel(chart));

		JFrame frame = new JFrame("chart " + id);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(this);
		frame.pack();
		frame.setLocation(xPos, yPos);
		frame.setVisible(true);
	}

	public void update(float value) {
		ts.addOrUpdate(new Millisecond(), value);
	}

	public Short getId() {
		return id;
	}
}