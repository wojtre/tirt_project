package Displaying;

import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import com.xuggle.xuggler.demos.VideoImage;

import Communication.Receiving.BufferedImageReceiver;
import Communication.Receiving.Service;

public class VideoDisplaing extends JFrame implements Service {

	private static final long serialVersionUID = -5233620382920646236L;

	private static final short CHART_ID = 2; // and chart_id + 2

	private VideoImage mScreen;

	private BufferedImageReceiver receiver;

	public VideoDisplaing(int inPort) {
		mScreen = new VideoImage();
		receiver = new BufferedImageReceiver(inPort, CHART_ID);
		receiver.addService(this);
		new Thread(receiver).start();

	}

	public void displayFrame(BufferedImage image) {
		mScreen.setImage(image);
	}

	@Override
	public void accept() {
		displayFrame(receiver.getImage());
	}

}
