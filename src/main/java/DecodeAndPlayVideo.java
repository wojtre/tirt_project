import Communication.Reading.VideoReader;
import Communication.Reading.VideoReaderImpl;
import Displaying.VideoDisplaing;
import Processing.ProcessFrame;
import Statistics.Chart;
import Statistics.StatisticsReceiver;

public class DecodeAndPlayVideo {

	private static final int READER_OUT_PORT = 9001;
	private static final int PROCESSING_IN_PORT = READER_OUT_PORT;

	private static final int PROCESSING_OUT_PORT = 9002;
	private static final int DISPLAING_IN_PORT = PROCESSING_OUT_PORT;

	private static final String OPENCV_PATH_WOJTEK = "C:\\Program Files\\Java\\opencv\\build\\java\\x64\\opencv_java2411.dll";
	private static final String OPENCV_PATH_ADRIAN = "C:\\Users\\Adrian\\Documents\\opencv\\build\\java\\x86\\opencv_java2410.dll";

	private static final String MOVIE_PATH_WOJTEK = "C:/Users/wojci/IdeaProjects/license-plate-recognition-system/src/main/resources/filmy/film1.avi";
	private static final String MOVIE_PATH_ADRIAN = "drop2.avi";

	private static final String MOVIE_PATH = MOVIE_PATH_ADRIAN;
	private static final String OPENCV_PATH = OPENCV_PATH_ADRIAN;

	public static void main(String[] args) {
		System.load(OPENCV_PATH);
		String filename = MOVIE_PATH;

		runStatReceiver();

		try {
			new ProcessFrame(PROCESSING_IN_PORT, PROCESSING_OUT_PORT);
			new VideoDisplaing(DISPLAING_IN_PORT);

			VideoReader videoDecoder = new VideoReaderImpl();
			videoDecoder.readVideo(filename, READER_OUT_PORT);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void runStatReceiver() {
		Chart chart = new Chart("Wielkosc odebranej ramki, processFrame",
				"czas", "liczba bajtow", (short) 1, 0, 300);

		Chart chart2 = new Chart("Wielkosc odebranej ramki, displayFrame",
				"czas", "liczba bajtow", (short) 2, 800, 300);

		Chart chart3 = new Chart("BitRate, processFrame", "czas",
				"liczba bajtow", (short) 3, 0, 600);

		Chart chart4 = new Chart("BitRate, displayFrame", "czas",
				"liczba bajtow", (short) 4, 800, 600);

		StatisticsReceiver statReceiver = new StatisticsReceiver();
		statReceiver.addChart(chart);
		statReceiver.addChart(chart2);
		statReceiver.addChart(chart3);
		statReceiver.addChart(chart4);
		new Thread(statReceiver).start();
	}
}
